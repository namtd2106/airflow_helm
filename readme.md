### First you need Argocd install via Helm:
```bash
SOURCE_CODE=.

helm repo add argo https://argoproj.github.io/argo-helm

helm install argocd argo/argo-cd --namespace argocd --create-namespace -f $SOURCE_CODE/argocd/values.yaml
```

### You need to create airflow namespace before add airflow argocd application
```bash
kubectl create namespace airflow
```

### Then you add airflow application for argocd:
```bash
kubectl apply -f $SOURCE_CODE/argocd/airflow_app.yaml
```

### ArgoCD will then auto deploy Airflow on namespace airflow. 
### Enjoy your Cluster!!!


#Tạo secrets
kubectl create secret generic git-credentials -n airflow --from-literal=GIT_SYNC_USERNAME='' --from-literal=GIT_SYNC_PASSWORD=''

# restart deployment trên 1 name space
kubectl rollout restart deployment -n airflow

kubectl delete pod -n airflow

kubectl delete namespace airflow

kubectl get pod -A

kubectl describe pod -n airflow

kubectl port-forward svc/airflow-webserver 8080:8080 --namespace airflow

POD_NAME=airflow-scheduler-dc47cfff8-s998c
kubectl logs $POD_NAME -n airflow -c git-sync-init
